package zkspringjpa.test.entity;

import java.io.Serializable;

import javax.persistence.*;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Window;

import zkspringjpa.test.services.BookServices;

@Entity
@Table(name="book")
@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class Book implements Serializable, Cloneable{
	private static final long serialVersionUID = 1L;
	@Transient
	@WireVariable
	private BookServices bookService;
	@Id
	@GeneratedValue
	@Column(name="id")
	private Integer id;
	
	@Column(name="name")
	private String name;
	
	@Column(name="description")
	private String desciption;
	
	@Column(name="price")
	private double price;

	public Book() {
		super();
	}

	

	public Integer getId() {
		return id;
	}



	public void setId(Integer id) {
		this.id = id;
	}



	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDesciption() {
		return desciption;
	}

	public void setDesciption(String desciption) {
		this.desciption = desciption;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	
	public Book(BookServices bookService, Integer id, String name, String desciption, double price) {
		super();
		this.bookService = bookService;
		this.id = id;
		this.name = name;
		this.desciption = desciption;
		this.price = price;
	}



	@Command
	public void addBook(@BindingParam("book") Book book, @BindingParam("wd") Window wd,@BindingParam("vm") Object vm) {
		bookService.addBook(book);
		BindUtils.postNotifyChange(null, null, vm, "listBookModel");
		wd.detach();
	}
	@Command
	public void resetBook(@BindingParam("book") Book book,@BindingParam("wd") Window wd,@BindingParam("vm") Object vm) {
		System.out.println(vm);
		BindUtils.postNotifyChange(null, null, vm, "listBookModel");
		wd.detach();
		
	}
}
