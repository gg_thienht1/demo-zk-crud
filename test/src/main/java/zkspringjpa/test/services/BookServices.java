package zkspringjpa.test.services;

import java.util.List;

import zkspringjpa.test.entity.Book;

public interface BookServices {
	public List<Book> listAll();
	void delete(Book book);
	public List<Book> search(String keyword);
	Book addBook(Book book);
	public void edit(Book book);

}
