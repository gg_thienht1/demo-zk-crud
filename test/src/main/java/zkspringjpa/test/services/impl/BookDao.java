package zkspringjpa.test.services.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.zkoss.zul.ListModelList;

import zkspringjpa.test.entity.Book;
import zkspringjpa.test.entity.Log;

@Repository
public class BookDao {
	@PersistenceContext
	private EntityManager em;
	
	@Transactional
	public List<Book> listAll(){
		return em.createQuery("SELECT b FROM Book b",Book.class).getResultList();
	}
	
	@Transactional(readOnly = true)
	public Book get(Integer id) {
		return em.find(Book.class, id);
	}
	
	@Transactional
	public void delete(Book book) {
		Book r = get(book.getId());
		if(r != null) {
			em.remove(r);
		}
	}
	@Transactional
	public List<Book> search(String keyword) {
		System.out.println("ssssssssssssssssssssssss");
		return  em.createQuery("SELECT b FROM Book b where b.Name like "+keyword).getResultList();
		
		
	}
	
	@Transactional
	public Book save(Book book) {
		Book b =  new Book();
		b.setId(book.getId());
		b.setName(book.getName());
		b.setDesciption(book.getDesciption());
		b.setPrice(book.getPrice());
		if(book.getId()!=0) {
			em.merge(book);
		}else {
			em.persist(book);
		}
		
		em.flush();
		return book;
	}
	public Book editBook(int id) {
		return em.createQuery("UPDATE b FROM Book where id="+id,Book.class).getSingleResult();
	}
}
